﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;


namespace CourseProject
{
    public partial class RecentOrders : Form
    {
        public DataTable table = new DataTable("table");
        
        public RecentOrders()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { 
                table.Rows.Clear();
            } catch (NullReferenceException){}

            if (Db.OpenConnection())
            {
                String dt = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                MySqlCommand cmd = new MySqlCommand("", Db.connection);
                cmd.CommandText = "SELECT c.taxi_code, c.driver, c.car_number, c.brand, FORMAT(SUM(o.fare), 'C2', 'bg-BG') AS money FROM cars c " 
                    + "LEFT JOIN orders o ON o.taxi_code = c.taxi_code "
                    + "WHERE DATE_FORMAT(order_time, '%Y-%m-%d') <= @dt " 
                    + "GROUP BY c.taxi_code";
                cmd.Parameters.AddWithValue("@dt", dt);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    table.Rows.Add(dataReader["taxi_code"], dataReader["driver"], dataReader["car_number"], dataReader["brand"], dataReader["money"]);
                }
                dataReader.Close();
            }
        }

        private void DataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index;
            Console.WriteLine(table.Rows[index][0]);
        }

        private void RecentOrders_Load(object sender, EventArgs e)
        {

            table.Columns.Add("Taxi Code");
            table.Columns.Add("Driver");
            table.Columns.Add("Car Number");
            table.Columns.Add("Brand");
            table.Columns.Add("Money");

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToOrderColumns = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.ReadOnly = true;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dataGridView1.SelectionChanged += DataGridView1_SelectionChanged;

            dataGridView1.DataSource = table;
        }
    }
}
