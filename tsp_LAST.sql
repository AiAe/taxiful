-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.43-MariaDB-0ubuntu0.18.04.1 - Ubuntu 18.04
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table tsp2.cars
CREATE TABLE IF NOT EXISTS `cars` (
  `taxi_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `car_number` varchar(50) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `seats` int(11) DEFAULT '4',
  `luggage` enum('Yes','No') DEFAULT 'No',
  `driver` varchar(50) NOT NULL,
  PRIMARY KEY (`taxi_code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table tsp2.cars: ~5 rows (approximately)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` (`taxi_code`, `car_number`, `brand`, `seats`, `luggage`, `driver`) VALUES
	(1, 'B 6969 BB', 'Opel', 4, 'Yes', 'Стефан'),
	(2, 'B 6112 PB', 'BMW', 4, 'Yes', 'Евгени'),
	(3, 'B 1324 AM', 'Ford', 6, 'Yes', 'Атанас'),
	(4, 'B 4775 KT', 'Peugeot', 4, 'No', 'Денислав'),
	(5, 'B 9586 BH', 'Nissan', 4, 'Yes', 'Мирослав'),
	(6, 'H 6969 HH', 'Opel', 4, 'No', 'Daniel'),
	(7, 'H 1111 DD', 'Opel', 4, 'No', 'Иван');
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;

-- Dumping structure for table tsp2.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `taxi_code` int(11) unsigned NOT NULL,
  `address` varchar(50) NOT NULL,
  `order_time` datetime NOT NULL,
  `distance` float NOT NULL DEFAULT '0',
  `fare` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `taxi_code` (`taxi_code`),
  CONSTRAINT `taxi_code1` FOREIGN KEY (`taxi_code`) REFERENCES `cars` (`taxi_code`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table tsp2.orders: ~10 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `taxi_code`, `address`, `order_time`, `distance`, `fare`) VALUES
	(11, 1, 'ulica Karamfil 19A', '2019-12-06 16:48:19', 8.11, 5.55),
	(12, 3, 'bul. Tsar Osvoboditel 109', '2019-12-06 17:50:48', 4.22, 2),
	(13, 2, 'ul. "Studentska" 1', '2019-12-08 16:54:32', 9.49, 6.21),
	(14, 4, 'бул. 8-ми Приморски полк', '2019-12-08 16:55:29', 5, 3),
	(15, 5, 'ЖП гара Варна', '2019-12-09 16:58:53', 6.1, 3.3),
	(16, 1, 'ул. „Атанас Москов“ 2', '2019-12-10 17:00:02', 9, 6),
	(17, 4, ' Виница, М. Горна Трака', '2019-12-11 18:00:02', 22, 14.1),
	(18, 2, 'ul. Evlogi Georgiev 24', '2019-12-12 18:03:06', 6, 3.1),
	(19, 5, 'ul. "Akademik Andrei Saharov" 2', '2019-12-13 18:08:02', 2, 2.1),
	(20, 3, 'бул. „Владислав Варненчик“ 271', '2019-12-14 18:23:49', 4, 4),
	(21, 1, 'ulica Karamfil 19A', '2019-12-09 23:24:27', 8, 5);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
