﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseProject
{
    public partial class Cars : Form
    {
        public DataTable table = new DataTable("table");

        public Cars()
        {
            InitializeComponent();
            table.Columns.Add("Taxi Code");
            table.Columns.Add("Car Number");
            table.Columns.Add("Brand");
            table.Columns.Add("Seats");
            table.Columns.Add("Luggage");
            table.Columns.Add("Driver");

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToOrderColumns = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.ReadOnly = true;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dataGridView1.DataSource = table;

            dataGridView1.DoubleClick += DataGridView1_DoubleClick;

            if (Db.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand("", Db.connection);
                cmd.CommandText = "SELECT * FROM cars";
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    table.Rows.Add(dataReader["taxi_code"], dataReader["car_number"], dataReader["brand"], dataReader["seats"], dataReader["luggage"], dataReader["driver"]);
                }
                dataReader.Close();
            }
        }

        private void DataGridView1_DoubleClick(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index;
            CarOrders co = new CarOrders(int.Parse(table.Rows[index][0].ToString()));
            co.Show();
        }
    }
}
