﻿using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace CourseProject
{
    public partial class CarOrders : Form
    {
        public DataTable table = new DataTable("table");
        public CarOrders(int taxi_code, bool code = true)
        {
            InitializeComponent();

            table.Columns.Add("Taxi Code");
            table.Columns.Add("Address");
            table.Columns.Add("Order Time");
            table.Columns.Add("Distance");
            table.Columns.Add("Fare");

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToOrderColumns = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.ReadOnly = true;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dataGridView1.DataSource = table;

            if (Db.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand("", Db.connection);

                if(code) 
                { 
                    cmd.CommandText = "SELECT * FROM orders WHERE taxi_code = @tc";
                    cmd.Parameters.AddWithValue("@tc", taxi_code);
                } else
                {
                    cmd.CommandText = "SELECT * FROM orders";
                }

                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    table.Rows.Add(dataReader["taxi_code"], dataReader["address"], dataReader["order_time"], dataReader["distance"], dataReader["fare"]);
                }
                dataReader.Close();
            }
        }
    }
}
