﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace CourseProject
{
    public partial class AddCar : Form
    {
        public AddCar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string car_number = null;
            string brand = null;
            int seats = 0;
            string luggage = "No";
            string driver = null;

            if (!System.Text.RegularExpressions.Regex.IsMatch(textBox2.Text, @"^[A-Z]{1,2}\s[0-9]{4}\s[A-Z]{2}$"))
            {
                MessageBox.Show("Please enter valid car number.");
            }
            else
            {
                car_number = textBox2.Text.ToString();
            }

            if(textBox3.Text.Trim() == "")
            {
                MessageBox.Show("Please enter car brand.");
            } 
            else
            {
                brand = textBox3.Text;
            }

            if(!int.TryParse(textBox4.Text, out seats))
            {
                MessageBox.Show("Please enter only numbers.");
            }

            if (!(seats >= 3 && seats <= 10))
            {
                MessageBox.Show("Seats must be in range 3 to 10.");
            }

            if (checkBox1.Checked == true)
            {
                luggage = "Yes";
            }

            if(textBox5.Text.Trim() == "")
            {
                MessageBox.Show("Please enter driver name.");
            } else
            {
                driver = textBox5.Text;
            }

            if (car_number != null && brand != null && seats != 0 && driver != null) 
            {
                if (Db.OpenConnection() == true)
                {
                    MySqlCommand check_Car_Number = new MySqlCommand("", Db.connection);
                    check_Car_Number.CommandText = "SELECT taxi_code FROM cars WHERE car_number = @car_number";
                    check_Car_Number.Parameters.AddWithValue("@car_number", car_number);
                    MySqlDataReader reader = check_Car_Number.ExecuteReader();
                    if (reader.HasRows) 
                    {
                        MessageBox.Show("Car Exists!");
                        reader.Close();
                    } 
                    else 
                    {
                        reader.Close();
                        MySqlCommand sql = new MySqlCommand("", Db.connection);
                        sql.CommandText = "INSERT INTO cars (car_number, brand, seats, luggage, driver) VALUES (@car_number, @brand, @seats, @luggage, @driver)";
                        sql.Parameters.AddWithValue("@car_number", car_number);
                        sql.Parameters.AddWithValue("@brand", brand);
                        sql.Parameters.AddWithValue("@seats", seats);
                        sql.Parameters.AddWithValue("@luggage", luggage);
                        sql.Parameters.AddWithValue("@driver", driver);
                        sql.ExecuteNonQuery();
                   
                        MessageBox.Show("Car added!");
                        this.Close();
                    }
                }
                else 
                {
                    MessageBox.Show("Failed to connect to database!");
                }
            } 
            else 
            {
                MessageBox.Show("Empty fields found.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
