﻿namespace CourseProject
{
    partial class App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.въвежданеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаКолаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаПоръчкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поръчкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.формуляриToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.колиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поръчкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.изходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.въвежданеToolStripMenuItem,
            this.справкиToolStripMenuItem,
            this.формуляриToolStripMenuItem,
            this.изходToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(326, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // въвежданеToolStripMenuItem
            // 
            this.въвежданеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаКолаToolStripMenuItem,
            this.новаПоръчкаToolStripMenuItem});
            this.въвежданеToolStripMenuItem.Name = "въвежданеToolStripMenuItem";
            this.въвежданеToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.въвежданеToolStripMenuItem.Text = "Въвеждане";
            // 
            // новаКолаToolStripMenuItem
            // 
            this.новаКолаToolStripMenuItem.Name = "новаКолаToolStripMenuItem";
            this.новаКолаToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.новаКолаToolStripMenuItem.Text = "Нова кола";
            this.новаКолаToolStripMenuItem.Click += new System.EventHandler(this.новаКолаToolStripMenuItem_Click);
            // 
            // новаПоръчкаToolStripMenuItem
            // 
            this.новаПоръчкаToolStripMenuItem.Name = "новаПоръчкаToolStripMenuItem";
            this.новаПоръчкаToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.новаПоръчкаToolStripMenuItem.Text = "Нова поръчка";
            this.новаПоръчкаToolStripMenuItem.Click += new System.EventHandler(this.новаПоръчкаToolStripMenuItem_Click);
            // 
            // справкиToolStripMenuItem
            // 
            this.справкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поръчкиToolStripMenuItem});
            this.справкиToolStripMenuItem.Name = "справкиToolStripMenuItem";
            this.справкиToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.справкиToolStripMenuItem.Text = "Справки";
            // 
            // поръчкиToolStripMenuItem
            // 
            this.поръчкиToolStripMenuItem.Name = "поръчкиToolStripMenuItem";
            this.поръчкиToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.поръчкиToolStripMenuItem.Text = "Поръчки";
            this.поръчкиToolStripMenuItem.Click += new System.EventHandler(this.поръчкиToolStripMenuItem_Click);
            // 
            // формуляриToolStripMenuItem
            // 
            this.формуляриToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.колиToolStripMenuItem,
            this.поръчкиToolStripMenuItem1});
            this.формуляриToolStripMenuItem.Name = "формуляриToolStripMenuItem";
            this.формуляриToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.формуляриToolStripMenuItem.Text = "Формуляри";
            // 
            // колиToolStripMenuItem
            // 
            this.колиToolStripMenuItem.Name = "колиToolStripMenuItem";
            this.колиToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.колиToolStripMenuItem.Text = "Коли";
            this.колиToolStripMenuItem.Click += new System.EventHandler(this.колиToolStripMenuItem_Click);
            // 
            // поръчкиToolStripMenuItem1
            // 
            this.поръчкиToolStripMenuItem1.Name = "поръчкиToolStripMenuItem1";
            this.поръчкиToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.поръчкиToolStripMenuItem1.Text = "Поръчки";
            this.поръчкиToolStripMenuItem1.Click += new System.EventHandler(this.поръчкиToolStripMenuItem1_Click);
            // 
            // изходToolStripMenuItem
            // 
            this.изходToolStripMenuItem.Name = "изходToolStripMenuItem";
            this.изходToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.изходToolStripMenuItem.Text = "Изход";
            this.изходToolStripMenuItem.Click += new System.EventHandler(this.изходToolStripMenuItem_Click);
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 121);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "App";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.App_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem въвежданеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem изходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаКолаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаПоръчкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поръчкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem формуляриToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem колиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поръчкиToolStripMenuItem1;
    }
}

