﻿using System;
using System.Windows.Forms;

namespace CourseProject
{
    public partial class App : Form
    {
        public App()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Db.Init();
        }

        private void изходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void поръчкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RecentOrders ro = new RecentOrders();
            ro.Show();
        }

        private void новаКолаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddCar ac = new AddCar();
            ac.Show();
        }

        private void новаПоръчкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddOrder ao = new AddOrder();
            ao.Show();
        }

        private void App_FormClosed(object sender, FormClosedEventArgs e)
        {
            Db.connection.Close();
            Console.WriteLine("bye");
        }

        private void колиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cars cars = new Cars();
            cars.Show();
        }

        private void поръчкиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CarOrders cars = new CarOrders(0, false);
            cars.Show();
        }
    }
}
